class BooksController < ApplicationController
before_filter :require_login, :only => [:new, :create, :edit, :update, :destroy]
  
  def show
    @book = Book.find(params[:id])
    @comment = Comment.new
    @comments = @book.comment
  end

  def new
    @book = Book.new  
  end

  def create
    @book= Book.new(params[:book])
    if @book.save
      flash[:notice] = "Book successfully saved"
      redirect_to :action => :index
    else
      flash.now[:notice] = "Book failed to saved"
      render :action => :new
    end
  end
  
  def edit
    @book = Book.find(params[:id])
  end

  def update
    @book = Book.find(params[:id])
    if @book.update_attributes(params[:book])
      redirect_to books_path, :notice => 'Book was successfully updated.'
    else
      flash[:error] = 'Book was failed to update.'
      render :action => 'edit'
    end
  end

  def index
    @books = Book.all
  end

  def destroy
    Book.destroy(params[:id])
    redirect_to books_path, :notice => 'Book was successfully deleted'
  end

end
