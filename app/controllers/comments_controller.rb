class CommentsController < ApplicationController
before_filter :require_login, :only => [:create, :destroy]
  
  def index
    @comments = Comment.all
    respond_to do |format|
      format.html # index.html.erb
      format.rss
    end
  end
  
  def create
    @book = Book.find_by_id(params[:comment][:book_id])
    @comment = @book.comments.create(params[:comment])
    @comments = @book.comments
    respond_to do |format|
      if @comment.save
        format.html { redirect_to(book_path(params[:comment][:book_id]), :notice => 'Comment was successfully created.') }
        format.js
      end
    end
  end

  def destroy
    @comment = Comment.find_by_id(params[:id])
    if @comment.destroy
      flash[:notice] = "Your comment has been deleted"
      redirect_to book_path(params[:book_id])
    else
      flash[:error] = "Your comment is failed to create"
      redirect_to book_path(params[:book_id])
    end
  end
end
