class Comment < ActiveRecord::Base
  belongs_to :article
  belongs_to :book
  
  attr_accessible :book_id, :comment, :user_id
  validates :comment, :presence => true
end
